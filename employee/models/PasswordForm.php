<?php
    namespace employee\models;
   
    use Yii;
    use yii\base\Model;
    use company\models\User;
   
    class PasswordForm extends Model{
        public $oldpass;
        public $newpass;
        public $repeatnewpass;
       
        public function rules(){
            return [
                [['oldpass','newpass','repeatnewpass'],'required'],
                ['oldpass','findPasswords'],
                [['newpass'], 'string', 'max' => 60, 'min' => 6],
                ['repeatnewpass','compare','compareAttribute'=>'newpass'],
            ];
        }
       
        public function findPasswords($attribute, $params){
            $user = User::find()->where([
                'username'=>Yii::$app->user->identity->username
            ])->one();
            if(!$user->validatePassword($this->oldpass))
                $this->addError($attribute,'Old password is incorrect');
        }
       
        public function attributeLabels(){
            return [
                'oldpass'=>'Password Saat Ini',
                'newpass'=>'Password Baru',
                'repeatnewpass'=>'Ulangi Password Baru',
            ];
        }
    }