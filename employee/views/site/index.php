<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
Yii::$app->params['showTitle'] = false;
?>
<div class="jumbotron" style="opacity:0.4">
    <h1><b><?= Yii::$app->user->identity->name ?></b></h1>
    <!-- <p class="lead"><?= Yii::$app->name ?></p> -->
</div>

<div class="detail-view-container">
<?= DetailView::widget([
    'options' => ['class' => 'table detail-view'],
    'model' => $model,
    'attributes' => [
        // 'id',
        'name',
        'username',
        // 'password_hash',
        'company.name:text:Company',
        'grade.name:text:Grade',
        'sex:integer',
        'date_of_birth',
        'place_of_birth',
        'address',
        'phone',
        // 'created_at:datetime',
        // 'updated_at:datetime',
        // 'createdBy.username:text:Created By',
        // 'updatedBy.username:text:Updated By',
    ],
]) ?>
</div>
