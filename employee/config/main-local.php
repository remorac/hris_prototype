<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'LHRxLOTal29YazopvJWdCOqfemployee',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '172.19.0.1', '172.20.0.1', '172.18.0.1'],
    ];

    $config['bootstrap'][] = 'gii';        
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '172.19.0.1', '172.20.0.1', '172.18.0.1'],
        'generators' => [
            'modelBehavior' => [
                'class' => 'backend\gii\model\Generator',
                'templates' => [
                    'modelBehavior' => '@backend/gii/model/default',
                ]
            ],
            'adminLteCrud' => [
                'class' => 'backend\gii\crud\Generator',
                'templates' => [
                    'adminLteCrud' => '@backend/gii/crud/default',
                ]
            ],
            'migration' => [
                'class' => 'backend\gii\migration\Generator',
                'templates' => [
                    'adminLteCrud' => '@backend/gii/migration/default',
                ]
            ],
            'mvc' => [
                'class' => 'backend\gii\mvc\Generator',
                'templates' => [
                    'adminLteCrud' => '@backend/gii/mvc/default',
                ]
            ],
        ],
    ];
}

return $config;