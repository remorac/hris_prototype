<?php

namespace frontend\controllers;

use Yii;
use backend\models\Research;
use backend\models\ResearchSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

/**
 * ResearchController implements the CRUD actions for Research model.
 */
class ResearchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Research models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ResearchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Research model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Research model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Research();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Research model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Research model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the Research model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Research the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Research::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    public function generatePdf($title, $view, $params = [], $landscape = false) {

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => 'A4',
            'orientation' => $landscape ? 'L' : 'P',
            'marginTop' => '20',          
            'filename' => $title,
            'destination' => Pdf::DEST_DOWNLOAD,
            'options' => ['title' => $title],
            'content' => $this->renderPartial($view, $params),
            'methods' => [
                // 'SetHeader' => \backend\helpers\ReportHelper::header($title, 1),
                'SetHeader' => [Yii::$app->name.'||'.$params['model']->id],
                'SetFooter' => ['|Page {PAGENO} of {nbpg}|'],
            ],
            'cssInline' => '
                .table-report th {text-align:center} 
                .table-report th, .table-report td {border:1px solid #aaa; padding:2px 4px;} 
                .table-report td {vertical-align:top}
            ',
        ]);
        return $pdf->render();
    }

    public function actionPdf($id) {
        $model = $this->findModel($id);

        return $this->generatePdf($model->id.'.pdf', 'pdf', [
            'model' => $model,
        ]);
    }

    public function actionImport()
    {
        if ($post = Yii::$app->request->post()) {
            
            $packageFile    = UploadedFile::getInstanceByName('package-file');
            $reader         = ReaderFactory::create(Type::XLSX);
            $reader->open($packageFile->tempName);
            
            foreach ($reader->getSheetIterator() as $sheet) {
                $rowCount = 0;
                foreach ($sheet->getRowIterator() as $row) {
                    $rowCount++;
                    if ($rowCount >= 2) {
                        $model              = new Research();
                        $model->name        = (string)$row[1];
                        $model->date        = (gettype($row[2]) == 'string') ? (string)$row[2] : $row[2]->format('Y-m-d');
                        $model->researcher  = (string)$row[3];
                        $model->description = (string)$row[4];

                        $model->save();
                    }
                }
            }
            $reader->close();
            return $this->redirect(['index']);
        }
        return $this->render('import');
    }
}