<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\News */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p class="small">
        <i>Posted at <?= Yii::$app->formatter->asDatetime($model->updated_at).' by '.$model->updatedBy->username; ?></i>
    </p>
    <?= $model->content?>

</div>
