<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Research */

$this->title = 'Create Research';
$this->params['breadcrumbs'][] = ['label' => 'Researches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="research-create boxxx boxxx-success">
	<div class="boxxx-header"></div>

    <div class="boxxx-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
