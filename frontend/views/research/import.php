<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Import Research');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Researches'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boxxx">

	<div class="row">
    <div class="package-form col-md-6">

	    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

	    <div class="form-group">
	    <?php 
	        echo FileInput::widget([
	            'id' => 'package-file',
	            'name' => 'package-file',
	            'options' => ['accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
	        ]);
	    ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>
	</div>

	<style type="text/css">
		.table-excel {
			font-family: sans-serif;
		}
		.table-excel th {
			text-align: center;
			color: #999;
		}
		.table-excel tr.data-header td {
			/*font-weight: bold;*/
			text-align: center;
			background: #ff06;
		}
	</style>

	<p>Format file excel yang didukung: </p>
	<center>
	<table class="table table-bordered table-condensed table-excel small text-muted" style="background: #fff">
		<tr>
			<th width="40px" style="background: #e7e7e7">&nbsp;</th>
			<th width="50px">A</th>
			<th>B</th>
			<th>C</th>
			<th>D</th>
			<th width="40%">E</th>
		</tr>
		<tr class="data-header">
			<th>1</th>
			<td>No</td>
			<td>Judul Penelitian</td>
			<td>Tanggal</td>
			<td>Nama Peneliti</td>
			<td>Abstrak</td>
		</tr>
		<tr>
			<th>2</th>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<th>3</th>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</table>
	</center>

	<p>
		Keterangan:
		<ul>
			<li>Data dimulai pada row 2 dan seterusnya (row 1 sebagai header).</li>
			<li>Format data kolom <b>Tanggal</b> berupa date, kecuali untuk file hasil export dari aplikasi ini dapat menggunakan format hasil export.</li>
		</ul>
	</p>

</div>