<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
Yii::$app->params['showTitle'] = false;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><b>cekjudul</b></h1>

        <p class="lead">find similar registered researches</p>

        <?php $form = ActiveForm::begin(['method' => 'get']); ?>
        <div class="col-md-6 col-md-offset-3">
            <div class="input-group" style="margin-bottom: 5px">
                <?= Html::activeTextInput($model, 'keyword', ['class' => 'form-control input-lg', 'placeholder' => 'keywords...', 'style' => 'border-color: lightgreen']) ?>
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-lg btn-success" style="padding: 10px 16px;font-size: 18px;"><i class="glyphicon glyphicon-search"></i></button>
                </span>
            </div>
            <!-- <span class="small text-muted text-center">The keywords above will be used to search title and description of registered researches.</span> -->
            <span class="small text-muted text-center">Masukkan kata kunci untuk menemukan data serupa pada Penelitian Terdaftar.</span>
            <p></p>
            <div class="col-md-12 text-muted font-weight-force-normal">
                <?php // echo Html::activeCheckbox($model, 'flag_name', ['label' => 'Title']) ?> &nbsp;&nbsp;&nbsp;
                <?php // echo Html::activeCheckbox($model, 'flag_description', ['label' => 'Description']) ?>                
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
