<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Ganti Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-changepassword boxxx boxxx-warning wow fadeInUp">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <div class="wow fadeIn" data-wow-delay="800ms">
    <p>Lengkapi data berikut untuk mengganti password: </p>
    <br>

    <?php $form = ActiveForm::begin([
        'id'=>'changepassword-form',
    ]); ?>

    <?= $form->field($model,'oldpass',['inputOptions'=>[
        'placeholder'=>''
    ]])->passwordInput() ?>
   
    <?= $form->field($model,'newpass',['inputOptions'=>[
        'placeholder'=>''
    ]])->passwordInput() ?>
   
    <?= $form->field($model,'repeatnewpass',['inputOptions'=>[
        'placeholder'=>''
    ]])->passwordInput() ?>
   
    <div class="form-group">
        <?= Html::submitButton('Ganti password',[
            'class'=>'btn btn-primary'
        ]) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
    </div>
</div>