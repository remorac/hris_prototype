<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->username;
// $this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-view boxxx boxxx-info">

    <div class="boxxx-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['profile-update'], [
            'class' => 'btn btn-warning',
        ]) ?>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                // 'username',
                // 'auth_key',
                // 'password_hash',
                // 'password_reset_token',
                'email:email',
                // 'status:integer',
                'student_id',
                'full_name',
                'date_of_birth',
                'place_of_birth',
                [
                    'attribute' => 'sex',
                    'value' => $model->sexText($model->sex),
                ],
                'phone',
                'address',
                // 'created_at:datetime',
                // 'updated_at:datetime',
            ],
        ]) ?>
        </div>
    </div>
</div>
