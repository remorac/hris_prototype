<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = 'Update Profile';
// $this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update boxxx boxxx-warning">

    <div class="boxxx-header"></div>

    <div class="boxxx-body">
        
        <div class="user-form">

            <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

            <?php // echo $form->field($model, 'username')->textInput(['maxlength' => true, 'readonly' => $model->isNewRecord ? false : true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly' => $model->isNewRecord ? false : true]) ?>

            <?= $form->field($model, 'student_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'date_of_birth')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'readonly' => true,
                'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
            ]); ?>

            <?= $form->field($model, 'place_of_birth')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'sex')->dropDownList(User::sexText('all'), ['prompt' => '']) ?>

            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>


            <div class="form-panel">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>

</div>
