<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProposalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proposal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'seminar_date') ?>

    <?= $form->field($model, 'file_registration_approval') ?>

    <?= $form->field($model, 'file_lecture_availability') ?>

    <?php // echo $form->field($model, 'file_invitation') ?>

    <?php // echo $form->field($model, 'file_handover') ?>

    <?php // echo $form->field($model, 'file_minutes_of_meeting') ?>

    <?php // echo $form->field($model, 'file_proposal') ?>

    <?php // echo $form->field($model, 'file_research_approval') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'verified_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
