<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */

$this->title = 'Seminar Proposal';
// $this->params['breadcrumbs'][] = ['label' => 'Proposal', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="proposal-view box-- box-info--">

    <div class="box-body--">

        <h4>PENDAFTARAN</h4>
        <p>
        <?php if (!$model->proposalLecturers) echo Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update-registration', 'id' => $model->id], [
            'class' => 'btn btn-default btn-text-warning',
        ]) ?>
        <?php /* if (!$model->proposalLecturers) echo Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Batalkan Pendaftaran', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-default btn-text-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */ ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                'id:text:No. Pendaftaran',
                // 'user.username:text:User',
                [
                    'attribute' => 'file_registration_approval',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_registration_approval ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_registration_approval'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_turnitin_proposal',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_turnitin_proposal ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_turnitin_proposal'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
            ],
        ]) ?>
        </div>
        

        <h4>PENENTUAN JADWAL</h4>
        <p>
        <?php if (!$model->seminar_date) echo Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update-scheduling', 'id' => $model->id], [
            'class' => 'btn btn-default btn-text-warning',
        ]) ?>
        </p>
        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                'lecturersName:html',
                [
                    'attribute' => 'file_lecturer_availability',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_lecturer_availability ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_lecturer_availability'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'seminar_date',
                    'format' => 'raw',
                    'value' => function($model) {
                        $return = $model->seminar_date;
                        if ($model->started_at) {
                            $return.= '<br><small>' . date('H:i', strtotime($model->started_at));
                            if ($model->ended_at) $return.= ' - ' . date('H:i', strtotime($model->ended_at));
                            $return.= '</small>';
                        }
                        return $return;
                    }
                ]
            ],
        ]) ?>
        </div>
            
        <h4>PRA SEMINAR</h4>
        <p>
        <?php if (!$model->verified_at) echo Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update-pre-seminar', 'id' => $model->id], [
            'class' => 'btn btn-default btn-text-warning',
        ]) ?>
        </p>
        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'file_invitation',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_invitation ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_invitation'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_handover',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_handover ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_handover'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
            ],
        ]) ?>
        </div>
        
        <h4>PASCA SEMINAR</h4>
        <p>
        <?php if (!$model->verified_at) echo Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update-pasca-seminar', 'id' => $model->id], [
            'class' => 'btn btn-default btn-text-warning',
        ]) ?>
        </p>
        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'file_minutes_of_meeting',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_minutes_of_meeting ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_minutes_of_meeting'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_proposal',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_proposal ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_proposal'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_research_approval',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_research_approval ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_research_approval'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                'lecturerAttendedsName:html',
            ],
        ]) ?>
        </div>

    </div>
</div>
