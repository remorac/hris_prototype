<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */

$this->title = 'Pendaftaran Seminar Proposal';
$this->params['breadcrumbs'][] = ['label' => 'Proposal', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
