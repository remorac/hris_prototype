<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Form */

$this->title = 'Update Form: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Form', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="form-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
