<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProposalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ujian Skripsi';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="proposal-index box-- box-primary-- box-body--">

    <?= Html::a('<i class="fa fa-plus"></i> ' . 'Daftar', ['register'], ['class' => 'btn btn-success']) ?>
    <p>
    </p>
    <div class="detail-view-container text-center text-muted" style="padding:50px 20px">
        <p>
        <big>Anda belum mendaftar Ujian Skripsi.</big>
        <br><span class="small">Klik tombol <code>Daftar</code> untuk memulai proses pendaftaran.</span>
        </p>
    </div>

</div>