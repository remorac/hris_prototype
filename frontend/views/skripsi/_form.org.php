<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\User;
use backend\models\Proposal;

/* @var $this yii\web\View */
/* @var $model backend\models\Skripsi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="skripsi-form">

<div class="row">
<div class="col-md-8 col-sm-12">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(User::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'proposal_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Proposal::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'examination_date')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); ?>

    <?= $form->field($model, 'toefl_score')->textInput() ?>

    <?= $form->field($model, 'file_approval')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_toefl')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_turnitin_article')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_turnitin_skripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_publishing')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_lecturer_availability')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_invitation')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_handover')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_minutes_of_meeting')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_article')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_skripsi')->textarea(['rows' => 6]) ?>

    
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-12">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

</div>
