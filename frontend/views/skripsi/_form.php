<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\User;
use backend\models\Proposal;

/* @var $this yii\web\View */
/* @var $model backend\models\Skripsi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="skripsi-form">

<div class="row">
<div class="col-md-8 col-sm-12">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php echo $form->field($model, 'toefl_score')->textInput() ?>

    <?php 
        $fields = [
            'file_approval',
            'file_toefl',
            'file_turnitin_article',
            'file_turnitin_skripsi',
            'file_publishing',
        ];
    ?>

    <?php foreach ($fields as $field) { ?>
    <div class="form-group">
        <label><?= $model->getAttributeLabel($field) ?></label>
        <?= Html::fileInput($field) ?>
    </div>
    <?php } ?>
        
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-12">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

</div>
