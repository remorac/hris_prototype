<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use backend\models\Lecturer;
use backend\models\SkripsiLecturerAttended;

/* @var $this yii\web\View */
/* @var $model backend\models\Skripsi */

$this->title = 'Update Pasca Seminar';
$this->params['breadcrumbs'][] = ['label' => 'Skripsi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="skripsi-form">

<div class="row">
<div class="col-md-8 col-sm-12">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php /* echo $form->field($model, 'seminar_date')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); */ ?>
    
    <?php 
        $fields = [
            'file_minutes_of_meeting',
            'file_article',
            'file_skripsi',
        ];
    ?>

    <?php foreach ($fields as $field) { ?>
    <div class="form-group">
        <label><?= $model->getAttributeLabel($field) ?></label>
        <?= Html::fileInput($field) ?>
    </div>
    <?php } ?>

    <?= $form->field($model, 'skripsi_title')->textInput() ?>

    <?= $form->field($model, 'skripsi_abstract')->textarea(['rows' => 6]) ?>
    
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-12">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Daftar' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>


    <br><br>
    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <label>Dosen yang Hadir</label>
        <table width="100%">
            <tr>
                <td>
                <?= Select2::widget([
                    'model' => $modelItem,
                    'attribute' => 'lecturer_id',
                    'data' => ArrayHelper::map(Lecturer::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => ['allowClear' => true],
                ]); ?>
                </td>
                <td style="width:1px; padding-left:5px"><?= Html::submitButton('<i class="glyphicon glyphicon-plus"></i> Tambah', ['class' => 'btn btn-success']); ?></td>
            </tr>
        </table>        
    </div>
    <?php ActiveForm::end(); ?>

    <div class="row">
        <div class="col-sm-12">
            <?php             
                $dataProvider = new \yii\data\ActiveDataProvider([
                    'query' => SkripsiLecturerAttended::find()->where(['skripsi_id' => $model->id]),
                    'pagination' => false,
                    /* 'sort' => [
                        'defaultOrder' => [
                            'id' => SORT_DESC,
                        ]
                    ], */
                ]);

                echo \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'pjax' => true,
                    'responsiveWrap' => false,
                    'hover' => true,
                    'striped' => false,
                    'bordered' => false,
                    'summary' => false,
                    'panel' => false,
                    'pjaxSettings' => ['options' => ['id' => 'grid']],
                    'tableOptions' => ['class' => 'table table-condensed'],
                    'rowOptions' => function($model) use ($modelItem) {
                        return $model->id == $modelItem->id ? ['style' => 'background:#ffff77'] : ['style' => ''];
                    },
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'headerOptions' => ['class' => 'text-right serial-column'],
                            'contentOptions' => ['class' => 'text-right serial-column'],
                        ],
                        [
                            'attribute' => 'lecturer_id',
                            'value' => 'lecturer.name',
                            'label' => 'Dosen',
                        ],
                        [
                            'contentOptions' => ['class' => 'action-column nowrap text-right'],
                            'attribute' => '',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return 
                                /* Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $data->proposal_id, 'proposal_lecturer_id' => $data->id], [
                                    'class' => 'btn btn-xs btn-default btn-text-warning',
                                ])
                                .' '. */
                                Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete-item', 'id' => $data->id], [
                                    'class' => 'btn btn-xs btn-default btn-text-danger',
                                    'data-confirm' => 'Are you sure you want to delete this item?',
                                    'data-method' => 'post',
                                ]);
                            }
                        ], 
                    ],
                ]);
            ?>
        </div>
    </div>


</div>
</div>

</div>
