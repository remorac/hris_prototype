<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Skripsi */

$this->title = 'Pendaftaran Ujian Skripsi';
$this->params['breadcrumbs'][] = ['label' => 'Skripsi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="skripsi-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
