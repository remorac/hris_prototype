<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Skripsi */

$this->title = 'Ujian Skripsi';
$this->params['breadcrumbs'][] = ['label' => 'Skripsi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="skripsi-view box-- box-info--">

    <div class="box-body--">
        <h4>PENDAFTARAN</h4>
        <p>
        <?php if (!$model->skripsiLecturers) echo Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update-registration', 'id' => $model->id], [
            'class' => 'btn btn-default btn-text-warning',
        ]) ?>
        <?php /* echo Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */ ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                'id:text:No. Pendaftaran',
                // 'user.full_name:text:User',
                // 'proposal.name:text:Proposal',
                'toefl_score',
                [
                    'attribute' => 'file_approval',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_approval ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_approval'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_toefl',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_toefl ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_toefl'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_turnitin_article',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_turnitin_article ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_turnitin_article'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_turnitin_skripsi',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_turnitin_skripsi ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_turnitin_skripsi'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_publishing',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_publishing ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_publishing'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
            ],
        ]) ?>
        </div>

        <h4>PENENTUAN JADWAL</h4>
        <p>
        <?php if (!$model->examination_date) echo Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update-scheduling', 'id' => $model->id], [
            'class' => 'btn btn-default btn-text-warning',
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                'lecturersName:html',
                [
                    'attribute' => 'file_lecturer_availability',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_lecturer_availability ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_lecturer_availability'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'examination_date',
                    'format' => 'raw',
                    'value' => function($model) {
                        $return = $model->examination_date;
                        if ($model->started_at) {
                            $return.= '<br><small>' . date('H:i', strtotime($model->started_at));
                            if ($model->ended_at) $return.= ' - ' . date('H:i', strtotime($model->ended_at));
                            $return.= '</small>';
                        }
                        return $return;
                    }
                ]
            ],
        ]) ?>
        </div>
        

        <h4>PRA UJIAN</h4>
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update-pre-examination', 'id' => $model->id], [
            'class' => 'btn btn-default btn-text-warning',
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'file_invitation',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_invitation ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_invitation'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_handover',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_handover ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_handover'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
            ],
        ]) ?>
        </div>


        <h4>PASCA UJIAN</h4>
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update-pasca-examination', 'id' => $model->id], [
            'class' => 'btn btn-default btn-text-warning',
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'file_minutes_of_meeting',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_minutes_of_meeting ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_minutes_of_meeting'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_article',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_article ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_article'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                [
                    'attribute' => 'file_skripsi',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-wrap'],
                    'value' => $model->file_skripsi ? Html::a('Download', ['download', 'id' => $model->id, 'field' => 'file_skripsi'], ['class' => 'btn btn-xs btn-default']) : null,
                ],
                'skripsi_title:ntext',
                'skripsi_abstract:ntext',
                'lecturerAttendedsName:html',
            ],
        ]) ?>
        </div>



    </div>
</div>
