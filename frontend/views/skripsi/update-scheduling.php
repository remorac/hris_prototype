<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Proposal */

$this->title = 'Update Scheduling';
$this->params['breadcrumbs'][] = ['label' => 'Skripsi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="proposal-form">

<div class="row">
<div class="col-md-8 col-sm-12">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php /* echo $form->field($model, 'seminar_date')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); */ ?>

    <?php 
        $fields = [
            'file_lecturer_availability',
        ];
    ?>

    <?php foreach ($fields as $field) { ?>
    <div class="form-group">
        <label><?= $model->getAttributeLabel($field) ?></label>
        <?= Html::fileInput($field) ?>
    </div>
    <?php } ?>

    <?php // $form->field($model, 'file_lecture_availability')->textarea(['rows' => 6]) ?>

    <?php // $form->field($model, 'file_invitation')->textarea(['rows' => 6]) ?>

    <?php // $form->field($model, 'file_handover')->textarea(['rows' => 6]) ?>

    <?php // $form->field($model, 'file_minutes_of_meeting')->textarea(['rows' => 6]) ?>

    <?php // $form->field($model, 'file_proposal')->textarea(['rows' => 6]) ?>

    <?php // $form->field($model, 'file_research_approval')->textarea(['rows' => 6]) ?>

    <?php // $form->field($model, 'remark')->textarea(['rows' => 6]) ?>

    <?php // $form->field($model, 'verified_at')->textInput() ?>

    
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-12">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Daftar' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

</div>
