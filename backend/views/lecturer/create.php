<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Lecturer */

$this->title = 'Create Lecturer';
$this->params['breadcrumbs'][] = ['label' => 'Lecturer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lecturer-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
