<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Lecturer */

$this->title = 'Update Lecturer: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Lecturer', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lecturer-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
