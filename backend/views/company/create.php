<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Company */

$this->title = 'Create Mitra';
$this->params['breadcrumbs'][] = ['label' => 'Mitra', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
