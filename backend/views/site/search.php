<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\Research;

$this->title = 'Search Result';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-search">
    
    <?php $form = ActiveForm::begin(['method' => 'get']); ?>
    <div class="row">
    <div class="col-md-12">
        <div class="input-group">
            <?= Html::activeTextInput($post, 'keyword', ['class' => 'form-control', 'placeholder' => 'keywords...', 'style' => 'border-color: lightgreen']) ?>
            <span class="input-group-btn">
                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i></button>
            </span>
        </div>
        <span class="small text-muted">The keywords above was used to search title and description of registered researches.</span>
        <p></p>
        <div class="col-md-12 text-muted font-weight-force-normal">
            <?php // echo Html::activeCheckbox($post, 'flag_name', ['label' => 'Title']) ?> &nbsp;&nbsp;&nbsp;
            <?php // echo Html::activeCheckbox($post, 'flag_description', ['label' => 'Description']) ?>
            
        </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

    <!-- <br> -->

    <?php

        // if ($post->flag_name) {
            $combinations   = [];
            $keywords       = array_unique(explode(' ', trim(preg_replace('/[^A-Za-z0-9\-\ ]/', '', $post->keyword))));
            $n              = count($keywords);

            for ($r = $n; $r >= 1 ; $r--) {
                // echo Research::factorial($n) / (Research::factorial($r) * Research::factorial($n - $r)) . ', ';
            }

            if ($n <= 0) {

                for ($r = $n; $r >= 1 ; $r--) {
                    $combinations[$n-$r] = Research::allSubsets($keywords, $r);
                }

                $models = [];
                foreach ($combinations as $combination) {
                    $params = '';
                    $param0 = [];
                    $query = Research::find();
                    foreach ($combination as $group) {
                        $param1 = [];
                        foreach ($group as $keyword) {
                            $param1[] = "(name like '%$keyword%' or description like '%$keyword%')";                        
                        }
                        $param0[] = implode(' and ', $param1);
                    }
                    $params = implode(' or ', $param0);
                    $query->orWhere($params);
                    foreach ($models as $existingModel) {
                        $query->andWhere(['!=', 'id', $existingModel['id']]);
                    }
                    $models = array_merge($models, $query->orderBy(['date' => SORT_DESC, 'id' => SORT_DESC])->asArray()->all());
                }

            } else {
                $query = Research::find();
                foreach ($keywords as $keyword) {
                    if ($keyword) $query->andWhere(['or', ['like', 'name', $keyword], ['like', 'description', $keyword]]);
                }
                $models = $query->orderBy(['date' => SORT_DESC, 'id' => SORT_DESC])->asArray()->all();
            }

            foreach ($models as $model) {
                $show_name = $model['name'];
                $show_desc = $model['description'];

                $plainkeys = implode('|', $keywords);
                // foreach ($keywords as $keyword) {
                //     if ($keyword) {
                        $show_name = preg_replace("/($plainkeys)/i", "<b class='text-red'>\$0</b>", $show_name);
                        $show_desc = preg_replace("/($plainkeys)/i", "<b class='text-red'>\$0</b>", $show_desc);
                //     }
                // }
                ?>
                <div class="detail-view-container" style="padding:15px; margin-bottom:15px">
                    <a href="<?= Url::to(['research/view', 'id' => $model['id']]) ?>">
                        <big><span class="text-primary"><?= $show_name ?></span></big>
                    </a>
                    <p class="small"><?= $show_desc ?></p>
                    <div class="small text-muted">
                        <span class="bdg"><?= date('d-M-Y', strtotime($model['date'])) ?></span>
                        <span class="bdg"><?= $model['researcher'] ?></span> 
                    </div>
                </div>
                <?php
            }

        // }
    ?>
</div>
