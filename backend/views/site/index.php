<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
Yii::$app->params['showTitle'] = false;
?>
<div class="site-index">

    <div class="jumbotron" style="opacity:0.4">
        <h1><b><?= Yii::$app->name ?></b></h1>
        <!-- <p class="lead"><?= Yii::$app->name ?></p> -->
    </div>

    <div class="row">
		<div class="col-sm-3">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3><?= \backend\models\Company::find()->count() ?></h3>
					<p>Mitra</p>
				</div>
				<div class="icon">
					<i class="fa fa-building"></i>
				</div>
				<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3><?= \backend\models\Employee::find()->count() ?></h3>
					<p>Pegawai</p>
				</div>
				<div class="icon">
					<i class="fa fa-street-view"></i>
				</div>
				<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3><?= Yii::$app->formatter->asDecimal(\backend\models\Payment::find()->sum('salary'), 0) ?></h3>
					<p>Total Pembayaran Gaji</p>
				</div>
				<div class="icon">
					<i class="fa fa-money"></i>
				</div>
				<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3><?= Yii::$app->formatter->asDecimal(\backend\models\Payment::find()->sum('allowance'), 0) ?></h3>
					<p>Total Pembayaran Tunjangan</p>
				</div>
				<div class="icon">
					<i class="fa fa-envelope"></i>
				</div>
				<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
	</div>
</div>
