<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $password_hash
 * @property integer $company_id
 * @property integer $grade_id
 * @property integer $sex
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property string $address
 * @property string $phone
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property Company $company
 * @property Grade $grade
 */
class Employee extends \yii\db\ActiveRecord
{
    public $password;
    
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /**
     * @inheritdoc
     */
    /* public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    } */
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'username', 'password_hash', 'company_id', 'grade_id'], 'required'],
            [['company_id', 'grade_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['date_of_birth'], 'safe'],
            [['name', 'username', 'password_hash', 'place_of_birth', 'address', 'phone'], 'string', 'max' => 255],
            [['sex'], 'string', 'max' => 4],
            [['username'], 'unique'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::className(), 'targetAttribute' => ['grade_id' => 'id']],

            [['password'], 'required', 'on' => self::SCENARIO_CREATE],
            [['password', 'role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nama',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'company_id' => 'Mitra',
            'grade_id' => 'Grade',
            'sex' => 'Jenis Kelamin',
            'date_of_birth' => 'Tanggal Lahir',
            'place_of_birth' => 'Tempat Lahir',
            'address' => 'Alamat',
            'phone' => 'Telp',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::className(), ['id' => 'grade_id']);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function getShortText() {
        return $this->name . ' - ' . $this->company->name;
    }
}
