<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 */
class User extends \common\models\User
{
    public $role;
    public $password;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /* public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['username', 'email', 'password', 'status'];
        $scenarios[self::SCENARIO_UPDATE] = ['username', 'email', 'password', 'status'];
        return $scenarios;
    } */

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            // \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    public static function statusText($index = 'all') {
        $array = [
            '10' => 'Active',
            '0' => 'Inactive',
        ];
        if (isset($array[$index])) return $array[$index];
        if ($index == 'all') return $array;
        return;
    }

    public static function sexText($index = 'all') {
        $array = [
            '1' => 'Laki-laki',
            '2' => 'Perempuan',
        ];
        if (isset($array[$index])) return $array[$index];
        if ($index == 'all') return $array;
        return;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['date_of_birth'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'auth_key'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['password'], 'required', 'on' => self::SCENARIO_CREATE],
            [['password', 'role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getRoles() 
    { 
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']); 
    } 
}
