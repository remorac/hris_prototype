<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Grade */

$this->title = 'Create Grade';
$this->params['breadcrumbs'][] = ['label' => 'Grade', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grade-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
